import java.util.StringTokenizer;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;
   private Node parentNode;


   Node(String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
      this.parentNode = null;
   }

   public void addChildren(Node firstChild) {
      Node child = firstChild;
      while (child != null) {
         child.setParentNode(this);
         child = child.nextSibling;
      }
   }

   public void setNextSibling(Node nextSibling) {
      this.nextSibling = nextSibling;
   }

   public void setParentNode(Node parentNode) {
      this.parentNode = parentNode;
   }

   public boolean hasParent() {
      return parentNode != null;
   }

   public boolean hasNextSibiling() {
      return nextSibling != null;
   }

   public boolean hasFirstChild() {
      return firstChild != null;
   }

   @Override
   public String toString() {
      return leftParentheticRepresentation();
   }

   public static Node parsePostfix(String s) {
      Node result = createNodeTree(isStringValid(s));
      if (!result.hasParent() && result.nextSibling != null) {
         throw new IllegalArgumentException("Node tree does not have a root in " + s);
      }
      return result;
   }

   public static String isStringValid(String s) {
      String sTrimmed = s.replaceAll("\\s+", "");
      if (sTrimmed.equals("")) {
         throw new IllegalArgumentException("Cannot have empty brackets in " + "\"" + s + "\"");
      } else if (sTrimmed.contains(",,")) {
         throw new IllegalArgumentException("Character \",,\" not allowed in " + "\"" + s + "\"");
      } else if (sTrimmed.contains("(,")) {
         throw new IllegalArgumentException("Character \",\" cannot be first in brackets in " + "\"" + s + "\"");
      } else if (sTrimmed.contains(",)")) {
         throw new IllegalArgumentException("Character \",\" cannot be last in brackets in " + "\"" + s + "\"");
      } else if (sTrimmed.contains("))")) {
         throw new IllegalArgumentException("Too many \")\" characters in " + "\"" + s + "\"");
      } else if (sTrimmed.contains("()")) {
         throw new IllegalArgumentException("Cannot have empty brackets in " + "\"" + s + "\"");
      }
      return s;
   }

   public static Node createNodeTree(String s) {
      StringTokenizer tree = new StringTokenizer(s, "(,)", true);
      Node lastNode = null;
      Node child = null;
      Node resultNode = null;
      boolean isFirstNode = true;
      boolean counting = false;
      StringBuilder toParse = new StringBuilder();
      int counter = 0;
      while (tree.hasMoreTokens()) {
         String token = tree.nextToken().trim();
         if (token.contains(" ")) {
            throw new IllegalArgumentException("Cannot have a space in the node name");
         } else if (token.equals("(") && !counting) {
            counting = true;
            counter++;
         } else if (token.equals("(")) {
            toParse.append(token);
            counter++;
         } else if (token.equals(")")) {
            counter--;
            if (counter == 0) {
               counting = false;
               child = Node.createNodeTree(toParse.toString());
               toParse = new StringBuilder();
            } else if (counter < 0) {
               throw new IllegalArgumentException("Too many \"" + token + "\" characters in " + "\"" + s + "\"");
            } else {
               toParse.append(token);
            }
         } else if (counting) {
            toParse.append(token);
         } else {
            if (!token.equals(",")) {
               if (isFirstNode) {
                  Node newNode = new Node(token, child, null);
                  if (child != null) {
                     newNode.addChildren(child);
                     child = null;
                  }
                  isFirstNode = false;
                  lastNode = newNode;
                  resultNode = newNode;
               } else {
                  Node newNode = new Node(token, child, null);
                  if (child != null) {
                     newNode.addChildren(child);
                     child = null;
                  }
                  lastNode.setNextSibling(newNode);
                  lastNode = newNode;
               }
            }
         }
      }
      if (counter > 0) {
         throw new IllegalArgumentException("Missing character \")\" from " + "\"" + s + "\"");
      }
      return resultNode;
   }

   public String leftParentheticRepresentation() {
      StringBuffer result = new StringBuffer();
      result.append(this.name != null ? this.name : "");
      result.append(this.firstChild != null ? "(" + this.firstChild.toString() + ")" : "");
      result.append(this.nextSibling != null ? "," + this.nextSibling.toString() : "");
      return result.toString();
   }

   public String pseudoXMLRepresentation() {
      return XMLString(1);
   }

   public String XMLString(int levelNumber) {
      String result = "";
      StringBuilder frontSpacing = new StringBuilder("\t".repeat(Math.max(0, levelNumber - 1)));
      StringBuilder backSpacing = new StringBuilder("\n");

      if (this.hasFirstChild() && !this.hasNextSibiling()) {
         String childrenXML = this.firstChild.XMLString(levelNumber + 1);
         result += (frontSpacing + "<L" + levelNumber + "> " + this.name + backSpacing + childrenXML + frontSpacing + "</L" + levelNumber + ">" + backSpacing);
      } else if (this.hasFirstChild() && this.hasNextSibiling()) {
         String childrenXML = this.firstChild.XMLString(levelNumber + 1);
         result += (frontSpacing + "<L" + levelNumber + "> " + this.name + backSpacing + childrenXML + frontSpacing + "</L" + levelNumber + ">" + backSpacing);
         result += this.nextSibling.XMLString(levelNumber);
      } else if (!this.hasFirstChild() && this.hasNextSibiling()) {
         String siblingXML = this.nextSibling.XMLString(levelNumber);
         result += (frontSpacing + "<L" + levelNumber + "> " + this.name + " </L" + levelNumber + ">" + backSpacing + siblingXML);
      } else {
         result += (frontSpacing + "<L" + levelNumber + "> " + this.name + " </L" + levelNumber + ">" + backSpacing);
      }
      return result;
   }

   public static void main(String[] param) {
      String s = "A,B";
      Node t = Node.parsePostfix(s);
      String v = t.leftParentheticRepresentation();
      System.out.println(s + " ==> " + v); // (B1,C)A ==> A(B1,C)
      System.out.println(t.pseudoXMLRepresentation());
   }
}

